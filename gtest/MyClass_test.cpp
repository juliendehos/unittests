#include "MyClass.hpp"

#include <gtest/gtest.h>

class MyClass_test : public MyClass { 
    public: 
        int f42_test() { return f42(); }
};

TEST (MyClass_test, f42) { 
    MyClass_test c;
    EXPECT_EQ (42, c.f42_test());
}

TEST (MyClass_test, f37) { 
    MyClass c;
    EXPECT_EQ (37, c.f37());
}

