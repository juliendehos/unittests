cmake_minimum_required( VERSION 3.0 )
project( unittests )

find_package(CxxTest)
if(CXXTEST_FOUND)
    file( GLOB TEST_SOURCE *_test.cpp )
    include_directories(${CXXTEST_INCLUDE_DIR})
    enable_testing()
    CXXTEST_ADD_TEST(unittests.out unittests.cpp ${TEST_SOURCE} MyClass.cpp)
endif()

