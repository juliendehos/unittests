#include "MyClass.hpp"
#include <cxxtest/TestSuite.h>

class MyClass_test : public CxxTest::TestSuite, public MyClass {

    public:

        void test_f42() {
            MyClass_test c;
            TS_ASSERT_EQUALS(42, c.f42());
        }

        void test_f37() {
            MyClass c;
            TS_ASSERT_EQUALS(37, c.f37());
        }

};

