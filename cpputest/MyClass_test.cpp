#include "MyClass.hpp"

#include <CppUTest/CommandLineTestRunner.h>

class MyClass_test : public MyClass {
    public: 
        int f42_test() { return f42(); }
};

TEST_GROUP( Group_MyClass) { };

TEST(Group_MyClass, Test_f42) {
    MyClass_test c;
	CHECK_EQUAL(42, c.f42_test());
}

TEST(Group_MyClass, Test_f37) {
    MyClass c;
	CHECK_EQUAL(37, c.f37());
}

